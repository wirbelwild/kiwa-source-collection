<?php

/**
 * Kiwa Source Collection. Handling HTML Audio, Picture and Video elements.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\SourceCollection\Tests;

use Generator;
use Kiwa\SourceCollection\AttributeCacheFileName;
use PHPUnit\Framework\TestCase;

class AttributeCacheFileNameTest extends TestCase
{
    public static function getGetAttributeCacheFileNameData(): Generator
    {
        yield [
            new AttributeCacheFileName('my-file.jpg'),
            'my-file-jpg-attributes.php',
        ];

        yield [
            new AttributeCacheFileName('/path/to/my-file.jpg'),
            'path-to-my-file-jpg-attributes.php',
        ];

        yield [
            new AttributeCacheFileName('.htaccess'),
            'htaccess-attributes.php',
        ];

        yield [
            new AttributeCacheFileName('/path/to/.htaccess'),
            'path-to-htaccess-attributes.php',
        ];

        yield [
            new AttributeCacheFileName('/path/to/ä_Weird.named.file'),
            'path-to-ä-weird-named-file-attributes.php',
        ];
    }

    /**
     * @dataProvider getGetAttributeCacheFileNameData
     * @param AttributeCacheFileName $attributeCacheFileName
     * @param string $attributeCacheFileNameExpected
     * @return void
     */
    public function testGetAttributeCacheFileName(AttributeCacheFileName $attributeCacheFileName, string $attributeCacheFileNameExpected): void
    {
        self::assertSame(
            $attributeCacheFileNameExpected,
            $attributeCacheFileName->getAttributeCacheFileName()
        );
    }
}
