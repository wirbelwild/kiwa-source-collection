<?php

/**
 * Kiwa Source Collection. Handling HTML Audio, Picture and Video elements.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\SourceCollection\Tests;

use DOMException;
use Kiwa\SourceCollection\Audio;
use PHPUnit\Framework\TestCase;

/**
 * Class AudioTest.
 */
class AudioTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        Helper::setUpBefore();
    }
    
    public static function tearDownAfterClass(): void
    {
        Helper::tearDownAfter();
    }

    /**
     * @runInSeparateProcess
     * @throws DOMException
     */
    public function testCanHandleAudioAutomatically(): void
    {
        Audio::enableAutoSearchGenerally(
            __DIR__
        );

        $audio = Audio::create(
            '/some/path/test-file.mp3',
            [
                'controls' => true,
                'muted' => true,
            ]
        );

        self::assertSame(
            '<audio controls="controls" muted="muted">
  <source src="/some/path/test-file.mp3" type="audio/mpeg"/>
  <source src="/some/path/test-file.ogg" type="audio/ogg"/>
</audio>',
            $audio->getAudio()
        );

        self::assertSame(
            '/some/path/test-file.mp3',
            $audio->getRootFileSrc()
        );

        self::assertEmpty(
            $audio->getRootFileOptions()
        );

        self::assertTrue(
            $audio->getAttributes()['controls']
        );
    }

    /**
     * @runInSeparateProcess
     * @throws DOMException
     */
    public function testCanHandleAudioManually(): void
    {
        $audio = Audio
            ::create(
                '/some/path/test-file.mp3',
                [
                    'controls' => true,
                    'muted' => true,
                ]
            )
            ->addSourceFile('/some/other/path/test-file.mp3')
            ->addSourceFile('/some/path/test-file.ogg')
        ;

        self::assertSame(
            '<audio controls="controls" muted="muted">
  <source src="/some/other/path/test-file.mp3" type="audio/mpeg"/>
  <source src="/some/path/test-file.mp3" type="audio/mpeg"/>
  <source src="/some/path/test-file.ogg" type="audio/ogg"/>
</audio>',
            $audio->getAudio()
        );
    }

    /**
     * @throws DOMException
     */
    public function testCanCreateDom(): void
    {
        $audio = new Audio(
            '/some/path/test-file.mp3',
            [
                'alt' => 'This is a description',
                'title' => 'This is a description',
            ]
        );

        $xml = $audio->getAudioDOMDocument();

        self::assertCount(
            1,
            $xml->getElementsByTagName('audio')
        );

        self::assertCount(
            1,
            $xml->getElementsByTagName('source')
        );

        self::assertSame(
            'This is a description',
            $xml->getElementsByTagName('audio')[0]->getAttribute('alt')
        );
    }
}
