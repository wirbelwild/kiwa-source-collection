<?php

return [
    'width' => 945,
    'height' => 945,
    'alt' => 'Bit&Black Logo',
    'title' => 'The Logo of Bit&Black',
    'loading' => 'lazy',
];
