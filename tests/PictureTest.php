<?php

/**
 * Kiwa Source Collection. Handling HTML Audio, Picture and Video elements.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\SourceCollection\Tests;

use DOMException;
use Kiwa\SourceCollection\Picture;
use PHPUnit\Framework\TestCase;

/**
 * Class PictureTest.
 */
class PictureTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        Helper::setUpBefore();
    }

    public static function tearDownAfterClass(): void
    {
        Helper::tearDownAfter();
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disable
     * @throws DOMException
     */
    public function testCanHandleImageAutomatically(): void
    {
        Picture::enableAutoSearchGenerally(
            __DIR__
        );

        $picture = new Picture(
            '/some/path/test-file.jpg',
            [
                'alt' => 'This is a description',
                'title' => 'This is a description',
            ]
        );

        self::assertSame(
            '<picture>
  <source srcset="/some/path/test-file.avif" type="image/avif"/>
  <source srcset="/some/path/test-file.webp" type="image/webp"/>
  <source srcset="/some/path/test-file.jpg" type="image/jpeg"/>
  <img src="/some/path/test-file.jpg" alt="This is a description" title="This is a description"/>
</picture>',
            $picture->getPicture()
        );

        self::assertSame(
            '/some/path/test-file.jpg',
            $picture->getRootFileSrc()
        );

        self::assertSame(
            'This is a description',
            $picture->getRootFileOptions()['alt']
        );

        self::assertEmpty(
            $picture->getAttributes()
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disable
     * @throws DOMException
     */
    public function testCanHandleImageManually1(): void
    {
        $picture = Picture::create(
            '/some/path/test-file.jpg',
            [
                'alt' => 'This is a description',
                'title' => 'This is a description',
            ]
        )
            ->addSourceFile('/some/path/test-file.webp', [
                'media' => '(min-width: 600px)',
            ])
        ;

        self::assertSame(
            '<picture>
  <source srcset="/some/path/test-file.webp" type="image/webp" media="(min-width: 600px)"/>
  <source srcset="/some/path/test-file.jpg" type="image/jpeg"/>
  <img src="/some/path/test-file.jpg" alt="This is a description" title="This is a description"/>
</picture>',
            $picture->getPicture()
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disable
     * @throws DOMException
     */
    public function testCanHandleImageManually2(): void
    {
        $picture = Picture::create(
            '/some/path/test-file.jpg',
            [
                'alt' => 'This is a description',
                'title' => 'This is a description',
            ]
        )
            ->addSourceFile('/some/path/test-file.jpg', [
                'media' => '(min-width: 600px)',
            ])
            ->addSourceFile('/some/path/test-file.webp', [
                'media' => '(min-width: 600px)',
            ])
        ;

        self::assertSame(
            '<picture>
  <source srcset="/some/path/test-file.webp" type="image/webp" media="(min-width: 600px)"/>
  <source srcset="/some/path/test-file.jpg" type="image/jpeg" media="(min-width: 600px)"/>
  <img src="/some/path/test-file.jpg" alt="This is a description" title="This is a description"/>
</picture>',
            $picture->getPicture()
        );
    }
    
    /**
     * @runInSeparateProcess
     * @preserveGlobalState disable
     * @throws DOMException
     */
    public function testCanHandleImageManually3(): void
    {
        $picture = Picture
            ::create(
                '/some/path/test-file.jpg',
                [
                    'alt' => 'This is a description',
                    'title' => 'This is a description',
                ]
            )
            ->addSourceFile('/some/path/test-file.jpg 1x, /some/path/test-file-hq.jpg 2x', [
                'media' => '(min-width: 600px)',
                'type' => 'image/jpeg',
            ])
            ->addSourceFile('/some/path/test-file.webp 1x, /some/path/test-file-hq.webp 2x', [
                'media' => '(min-width: 600px)',
                'type' => 'image/webp',
            ])
        ;

        self::assertSame(
            '<picture>
  <source srcset="/some/path/test-file.jpg 1x, /some/path/test-file-hq.jpg 2x" type="image/jpeg" media="(min-width: 600px)"/>
  <source srcset="/some/path/test-file.webp 1x, /some/path/test-file-hq.webp 2x" type="image/webp" media="(min-width: 600px)"/>
  <img src="/some/path/test-file.jpg" alt="This is a description" title="This is a description"/>
</picture>',
            $picture->getPicture()
        );
    }

    /**
     * @throws DOMException
     */
    public function testCanCreateDom(): void
    {
        $picture = new Picture(
            '/some/path/test-file.jpg',
            [
                'alt' => 'This is a description',
                'title' => 'This is a description',
            ]
        );
        
        $xml = $picture->getPictureDOMDocument();

        self::assertCount(
            1,
            $xml->getElementsByTagName('picture')
        );
        
        self::assertCount(
            1,
            $xml->getElementsByTagName('source')
        );

        self::assertCount(
            1,
            $xml->getElementsByTagName('img')
        );

        self::assertSame(
            'This is a description',
            $xml->getElementsByTagName('img')[0]->getAttribute('alt')
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disable
     * @throws DOMException
     */
    public function testCanHandleGif(): void
    {
        Picture::enableAutoSearchGenerally(
            __DIR__
        );

        $picture = new Picture(
            '/some/path/test-file.gif',
            [
                'alt' => 'This is a description',
                'title' => 'This is a description',
            ]
        );

        self::assertSame(
            '<picture>
  <source srcset="/some/path/test-file.webp" type="image/webp"/>
  <source srcset="/some/path/test-file.gif" type="image/gif"/>
  <img src="/some/path/test-file.gif" alt="This is a description" title="This is a description"/>
</picture>',
            $picture->getPicture()
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disable
     * @throws DOMException
     */
    public function testCanHandleEncoding(): void
    {
        $picture = new Picture(
            '/some/path/test-file.jpg',
            [
                'attribute' => 'Cömé & Fìnd &amp; ßee – "everything"! <b>Yes</b>',
            ]
        );

        self::assertStringContainsString(
            'Cömé &amp; Fìnd &amp; ßee – &quot;everything&quot;! &lt;b&gt;Yes&lt;/b&gt;',
            $picture->getPicture()
        );
    }
}
