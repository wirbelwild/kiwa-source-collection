<?php

/**
 * Kiwa Source Collection. Handling HTML Audio, Picture and Video elements.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\SourceCollection\Tests;

use DOMException;
use Kiwa\SourceCollection\Video;
use PHPUnit\Framework\TestCase;

/**
 * Class VideoTest.
 */
class VideoTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        Helper::setUpBefore();
    }
    
    public static function tearDownAfterClass(): void
    {
        Helper::tearDownAfter();
    }

    /**
     * @runInSeparateProcess
     * @throws DOMException
     */
    public function testCanHandleVideoAutomatically(): void
    {
        Video::enableAutoSearchGenerally(
            __DIR__
        );

        $video = Video::create(
            '/some/path/test-file.mp4',
            [
                'width' => 250,
                'height' => 100,
                'muted' => true,
            ]
        );

        self::assertSame(
            '<video width="250" height="100" muted="muted">
  <source src="/some/path/test-file.mp4" type="video/mp4"/>
  <source src="/some/path/test-file.mov" type="video/quicktime"/>
</video>',
            $video->getVideo()
        );

        self::assertSame(
            '/some/path/test-file.mp4',
            $video->getRootFileSrc()
        );

        self::assertEmpty(
            $video->getRootFileOptions()
        );

        self::assertSame(
            250,
            $video->getAttributes()['width']
        );
    }

    /**
     * @runInSeparateProcess
     * @throws DOMException
     */
    public function testCanHandleVideoManually(): void
    {
        $video = Video
            ::create(
                '/some/path/test-file.mp4',
                [
                    'width' => 250,
                    'height' => 100,
                    'muted' => true,
                ]
            )
            ->addSourceFile('/some/other/path/test-file.mp4')
            ->addSourceFile('/some/path/test-file.mov')
        ;

        self::assertSame(
            '<video width="250" height="100" muted="muted">
  <source src="/some/other/path/test-file.mp4" type="video/mp4"/>
  <source src="/some/path/test-file.mp4" type="video/mp4"/>
  <source src="/some/path/test-file.mov" type="video/quicktime"/>
</video>',
            $video->getVideo()
        );
    }

    /**
     * @runInSeparateProcess
     * @throws DOMException
     */
    public function testCanHandleFalseValues(): void
    {
        $video = Video
            ::create(
                '/some/path/test-file.mp4',
                [
                    'width' => 250,
                    'height' => 100,
                    'muted' => false,
                    'chicken' => null,
                    'number' => 0,
                ]
            )
            ->addSourceFile('/some/other/path/test-file.mp4')
            ->addSourceFile('/some/path/test-file.mov')
        ;

        self::assertSame(
            '<video width="250" height="100" number="0">
  <source src="/some/other/path/test-file.mp4" type="video/mp4"/>
  <source src="/some/path/test-file.mp4" type="video/mp4"/>
  <source src="/some/path/test-file.mov" type="video/quicktime"/>
</video>',
            $video->getVideo()
        );
    }

    /**
     * @throws DOMException
     */
    public function testCanCreateDom(): void
    {
        $video = new Video(
            '/some/path/test-file.mp4',
            [
                'alt' => 'This is a description',
                'title' => 'This is a description',
            ]
        );

        $xml = $video->getVideoDOMDocument();

        self::assertCount(
            1,
            $xml->getElementsByTagName('video')
        );

        self::assertCount(
            1,
            $xml->getElementsByTagName('source')
        );

        self::assertSame(
            'This is a description',
            $xml->getElementsByTagName('video')[0]->getAttribute('alt')
        );
    }
}
