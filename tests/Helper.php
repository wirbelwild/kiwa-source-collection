<?php

/**
 * Kiwa Source Collection. Handling HTML Audio, Picture and Video elements.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\SourceCollection\Tests;

/**
 * Class Helper.
 */
class Helper
{
    /**
     * @var string[]
     */
    private static array $extensions = [
        'avif',
        'gif',
        'jpg',
        'mov',
        'mp3',
        'mp4',
        'ogg',
        'png',
        'webp',
    ];

    public static function setUpBefore(): void
    {
        foreach (self::$extensions as $extension) {
            file_put_contents(
                self::getFileName($extension),
                ''
            );
        }
    }
    
    public static function tearDownAfter(): void
    {
        foreach (self::$extensions as $extension) {
            $fileName = self::getFileName($extension);
            
            if (file_exists($fileName)) {
                unlink($fileName);
            }
        }
    }

    /**
     * @param string $extension
     * @return string
     */
    private static function getFileName(string $extension): string
    {
        return __DIR__ . DIRECTORY_SEPARATOR . 'test-file.' . $extension;
    }
}
