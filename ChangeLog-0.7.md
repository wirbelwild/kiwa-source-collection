# Changes in Kiwa Source Collection v0.7

## 0.7.1 2022-04-12

### Fixed

-   Fixed version number of `bitandblack/pathinfo`.

## 0.7.0 2022-04-12

### Changed

-   Improved source file handling to prevent duplication when adding files manually.