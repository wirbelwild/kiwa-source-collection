# Changes in Kiwa Source Collection v1.1

## 1.1.1 2022-05-13

### Fixed 

-   Fixed dependency problem with `symfony/mime` for PHP 7.4.

## 1.1.0 2022-04-12

### Changed

-   Improved source file handling to prevent duplication when adding files manually.