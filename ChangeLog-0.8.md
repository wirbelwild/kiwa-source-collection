# Changes in Kiwa Source Collection v0.8

## 0.8.1 2023-08-18

### Changed

-   Added the method `setRootFileOptions`.

## 0.8.0 2023-08-18

### Added

-   Added the methods `getRootFileSrc`, `getRootFileOptions` and `getAttributes`.