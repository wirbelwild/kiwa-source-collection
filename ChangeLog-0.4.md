# Changes in Kiwa Source Collection 0.4

## 0.4.2 2021-04-22

### Fixed

-   Attributes will be removed if they are `false` or `null`.

## 0.4.1 2021-04-13

### Fixed

-   Added missing method `getSourceFiles` to get a list of all files that have been found.

## 0.4.0 2021-04-08

### Changed 

-   Each file may lay in a different folder now.
-   The `AbstractSourceCollection` class is abstract now.