# Changes in Kiwa Source Collection v1.0

## 1.0.0 2022-03-11

### Changes

-   PHP >=7.4 is now required.
-   Dependencies have been updated.