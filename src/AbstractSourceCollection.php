<?php

/**
 * Kiwa Source Collection. Handling HTML Audio, Picture and Video elements.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\SourceCollection;

use BitAndBlack\PathInfo\PathInfo;
use DOMDocument;
use DOMException;
use Symfony\Component\Mime\MimeTypes;

/**
 * Class AbstractSourceCollection.
 * @see \Kiwa\SourceCollection\Tests\AbstractSourceCollectionTest
 */
abstract class AbstractSourceCollection
{
    protected static ?bool $autoSearchEnabledGlobally = null;

    private static bool $attributeCacheEnabled = false;

    private static ?string $attributeCacheFolder = null;

    private ?bool $autoSearchEnabled = null;

    private DOMDocument $domDocument;

    /**
     * These are the next-gen formats, that need to be displayed at first.
     * A browser will take the first possibility, and those should be the newest formats.
     *
     * @var array<int, string>
     */
    protected array $nextGenFormats = [];

    /**
     * @var array<int, string>|null
     */
    protected static ?array $defaultRootFolderGlobally = null;

    /**
     * @var array<int, string>|null
     */
    private ?array $rootFolder = null;

    protected string $rootFileSrc;

    /**
     * @var array<string, string|int|float|bool|null>
     */
    protected array $rootFileOptions = [];

    /**
     * @var array<string, array<string, string>>
     */
    private array $sourceFiles = [];

    /**
     * @var array<string, string|int|float|bool|null>
     */
    private array $attributes = [];

    private bool $isPicture;

    private bool $isVideo;

    private bool $isAudio;

    /**
     * AbstractSourceCollection constructor.
     */
    public function __construct()
    {
        $this->domDocument = new DOMDocument('1.0', 'UTF-8');
        $this->domDocument->formatOutput = true;
        $this->isAudio = $this instanceof Audio;
        $this->isPicture = $this instanceof Picture;
        $this->isVideo = $this instanceof Video;
    }

    /**
     * Returns the collection as XML.
     *
     * @param string $nodeName
     * @return DOMDocument
     * @throws DOMException
     */
    protected function getCollectionDOMDocument(string $nodeName): DOMDocument
    {
        $collection = $this->domDocument->createElement($nodeName);

        if (true === self::$attributeCacheEnabled) {
            $cacheFileName = (string) new AttributeCacheFileName($this->rootFileSrc);
            $cacheFilePath = self::$attributeCacheFolder . DIRECTORY_SEPARATOR . $cacheFileName;

            $attributesCached = null;

            if (true === file_exists($cacheFilePath)) {
                $attributesCached = include $cacheFilePath;
            }

            if (true === is_array($attributesCached)) {
                $this->rootFileOptions = array_merge($attributesCached, $this->rootFileOptions);
            }
        }

        foreach ($this->attributes as $attributeName => $attributeValue) {
            if (true === $attributeValue) {
                $attributeValue = $attributeName;
            }

            $collection->setAttribute($attributeName, (string) $attributeValue);
        }

        $pathInfo = new PathInfo($this->rootFileSrc);
        $dirName = $pathInfo->getDirName();
        $fileName = $pathInfo->getFileName();

        if ((true === self::$autoSearchEnabledGlobally && false !== $this->autoSearchEnabled)
            || (false !== self::$autoSearchEnabledGlobally && true === $this->autoSearchEnabled)
        ) {
            $folders = $this->rootFolder ?? $this->getAutoSearchFolders() ?? self::$defaultRootFolderGlobally ?? [];
            $filesSameNamed = [];

            foreach ($folders as $folder) {
                $files = glob($folder . DIRECTORY_SEPARATOR . $fileName . '.*');

                if (!is_array($files)) {
                    continue;
                }

                array_push($filesSameNamed, ...$files);
            }

            foreach ($filesSameNamed as $fileSameNamed) {
                $fileType = $this->getFileType($fileSameNamed);
                $fileTypes = explode('/', $fileType);

                $fileNameRelative = $dirName . DIRECTORY_SEPARATOR . PathInfo::createFromFile($fileSameNamed)->getBaseName();

                $isAudio = $this->isAudio && 'audio' === $fileTypes[0];
                $isImage = $this->isPicture && 'image' === $fileTypes[0];
                $isVideo = $this->isVideo && 'video' === $fileTypes[0];

                if ($isAudio || $isImage || $isVideo) {
                    $this->sourceFiles[$fileNameRelative] = [];
                }
            }
        }

        $sourceFilesNames = array_keys($this->sourceFiles);
        $sourceFileAlreadyExists = false;

        foreach ($sourceFilesNames as $sourceFilesName) {
            if ($this->str_contains($sourceFilesName, $this->rootFileSrc)) {
                $sourceFileAlreadyExists = true;
                break;
            }
        }

        if (false === $sourceFileAlreadyExists) {
            $this->sourceFiles[$this->rootFileSrc] = [];
        }

        $this->sourceFiles = $this->removeUselessCombinations($this->sourceFiles);
        $this->sourceFiles = $this->sortNextGenFormatAtFirst($this->sourceFiles);

        $sourceAttributeName = $this->isPicture ? 'srcset' : 'src';

        foreach ($this->sourceFiles as $src => $options) {
            $source = $this->domDocument->createElement('source');
            $source->setAttribute($sourceAttributeName, $src);
            $source->setAttribute('type', $this->getFileType($src));

            foreach ($options as $optionName => $optionValue) {
                $source->setAttribute($optionName, $optionValue);
            }

            $collection->appendChild($source);
        }

        if ($this->isPicture) {
            $img = $this->domDocument->createElement('img');
            $img->setAttribute('src', $this->rootFileSrc);

            foreach ($this->getRootFileOptions() as $attributeName => $attributeValue) {
                $attributeValue = $this->handleEncoding((string) $attributeValue);
                $img->setAttribute($attributeName, $attributeValue);
            }

            $collection->appendChild($img);
        }

        $this->domDocument->appendChild($collection);

        return $this->domDocument;
    }

    /**
     * Renders and returns the collection to HTML.
     *
     * @param string $nodeName
     * @return string
     * @throws DOMException
     */
    protected function getCollection(string $nodeName): string
    {
        return (string) $this
            ->getCollectionDOMDocument($nodeName)
            ->saveXML(
                $this->domDocument->documentElement
            )
        ;
    }

    /**
     * Enables that related files are automatically searched and added.
     *
     * @param string ...$rootFolder One or multiple paths to the folder(s) where the files are stored in.
     * @return AbstractSourceCollection
     */
    public function enableAutoSearch(string ...$rootFolder): self
    {
        $this->autoSearchEnabled = true;
        $this->rootFolder = array_values($rootFolder);
        return $this;
    }

    /**
     * Enables that related files are automatically searched and added.
     * This method takes place as a global setting.
     *
     * @param string ...$defaultRootFolder One or multiple paths to the folder(s) where the files are stored in.
     */
    public static function enableAutoSearchGenerally(string ...$defaultRootFolder): void
    {
        self::$autoSearchEnabledGlobally = true;
        self::$defaultRootFolderGlobally = array_values($defaultRootFolder);
    }

    /**
     * Returns the auto search folders that have been set for a specific media type.
     *
     * @return array<int, string>|null
     */
    abstract protected function getAutoSearchFolders(): ?array;

    /**
     * Disables that related files are automatically searched and added. This behaviour is disabled per default.
     * This method takes place as a global setting.
     */
    public static function disableAutoSearchGenerally(): void
    {
        self::$autoSearchEnabledGlobally = false;
    }

    /**
     * Disables that related files are automatically searched and added. This behaviour is disabled per default.
     */
    public function disableAutoSearch(): self
    {
        $this->autoSearchEnabled = false;
        return $this;
    }

    /**
     * Manually adds a source file.
     *
     * @param string $fileSrc                    The absolute or relative URL of the file.
     *                                           This path will be displayed when creating the HTML code.
     * @param array<string, string> $fileOptions All attributes for the file, like `media`, or `sizes`.
     *                                           The `type` and `src` attributes will be added automatically.
     * @return $this
     */
    public function addSourceFile(string $fileSrc, array $fileOptions = []): self
    {
        $this->sourceFiles[$fileSrc] = $fileOptions;
        return $this;
    }

    /**
     * Returns all files that have been found.
     *
     * @return array<string, array<string, string>>
     */
    public function getSourceFiles(): array
    {
        return $this->sourceFiles;
    }

    /**
     * Returns the file type based on its name.
     *
     * @param string $fileSrc
     * @return string
     */
    private function getFileType(string $fileSrc): string
    {
        $extension = PathInfo::createFromFile($fileSrc)->getExtension();
        $mimeTypes = MimeTypes::getDefault()->getMimeTypes((string) $extension);
        return $mimeTypes[0] ?? 'application/octet-stream';
    }

    /**
     * Sets attributes to the root element, removing attributes that are set to false or not set at all.
     *
     * @param array<string, string|int|float|bool|null> $attributes
     * @return $this
     */
    public function addAttributes(array $attributes): self
    {
        foreach ($attributes as $key => $attribute) {
            if (0 !== $attribute && empty($attribute)) {
                unset($attributes[$key]);
            }
        }

        $this->attributes = array_merge($this->attributes, $attributes);
        return $this;
    }

    /**
     * @param array<string, array<string, string>> $sources
     * @return array<string, array<string, string>>
     */
    private function sortNextGenFormatAtFirst(array $sources): array
    {
        uksort(
            $sources,
            function ($itemA, $itemB): int {
                $extensionA = PathInfo::createFromFile($itemA)->getExtension();
                $extensionB = PathInfo::createFromFile($itemB)->getExtension();

                $matchA = array_search($extensionA, $this->nextGenFormats, true);
                $matchB = array_search($extensionB, $this->nextGenFormats, true);

                if (false === $matchA && false === $matchB) {
                    return 0;
                }

                if (false === $matchA) {
                    return 1;
                }

                if (false === $matchB) {
                    return -1;
                }

                return $matchA - $matchB;
            }
        );

        return $sources;
    }

    /**
     * Removes useless image format combinations. For example, if the root file is a JPEG, it'd make
     * no sense to add a PNG also.
     *
     * @param array<string, array<string, string>> $sources
     * @return array<string, array<string, string>>
     */
    private function removeUselessCombinations(array $sources): array
    {
        $extension = PathInfo::createFromFile($this->rootFileSrc)->getExtension();
        $extension = strtolower((string) $extension);

        if ($this->isPicture) {
            if ('jpg' === $extension || 'jpeg' === $extension) {
                return $this->removeFilesByExtension($sources, ['png', 'gif']);
            }

            if ('png' === $extension) {
                return $this->removeFilesByExtension($sources, ['jpg', 'jpeg']);
            }

            if ('gif' === $extension) {
                return $this->removeFilesByExtension($sources, ['jpg', 'jpeg', 'png', 'avif']);
            }
        }

        return $sources;
    }

    /**
     * @param array<string, array<string, string>> $sources
     * @param array<int, string> $extensions
     * @return array<string, array<string, string>>
     */
    private function removeFilesByExtension(array $sources, array $extensions): array
    {
        foreach ($sources as $fileName => $options) {
            foreach ($extensions as $extension) {
                if ($this->str_ends_with($fileName, $extension)) {
                    unset($sources[$fileName]);
                }
            }
        }

        return $sources;
    }

    /**
     * Returns the path to the main source file.
     *
     * @return string
     */
    public function getRootFileSrc(): string
    {
        return $this->rootFileSrc;
    }

    /**
     * Sets the options of the root file. This depends on the element:
     * *   When using the {@see Audio} class, the option will be appended to the `<audio /`> node.
     * *   When using the {@see Picture} class, the option will be appended to the `<img /`> node.
     * *   When using the {@see Video} class, the option will be appended to the `<video /`> node.
     *
     * @param array<string, string|int|float|bool|null> $rootFileOptions
     * @return self
     */
    public function setRootFileOptions(array $rootFileOptions): self
    {
        $this->rootFileOptions = $rootFileOptions;
        return $this;
    }

    /**
     * Returns the options of the root file.
     *
     * @return array<string, string|int|float|bool|null>
     */
    public function getRootFileOptions(): array
    {
        return $this->rootFileOptions;
    }

    /**
     * @return array<string, string|int|float|bool|null>
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    /**
     * @param string $haystack
     * @param string $needle
     * @return bool
     * @todo Remove in PHP 8.0
     */
    private function str_ends_with(string $haystack, string $needle): bool
    {
        if ('' === $needle || $needle === $haystack) {
            return true;
        }

        if ('' === $haystack) {
            return false;
        }

        $needleLength = strlen($needle);

        return $needleLength <= strlen($haystack) && 0 === substr_compare($haystack, $needle, -$needleLength);
    }

    /**
     * @param string $haystack
     * @param string $needle
     * @return bool
     * @todo Remove in PHP 8.0
     */
    private function str_contains(string $haystack, string $needle): bool
    {
        return '' === $needle || false !== strpos($haystack, $needle);
    }

    /**
     * Encodes and decodes the attribute's values.
     *
     * @param string $value
     * @return string
     */
    private function handleEncoding(string $value): string
    {
        return html_entity_decode($value);
    }

    /**
     * Enables loading element's attributes from static files.
     *
     * The cache file name is getting created with and used from the {@see AttributeCacheFileName},
     * whereby the Kiwa Source Collection doesn't create those files.
     * You can do that by yourself and store whatever information you want to store.
     * The cache file has to be a PHP file that returns a named array, for example:
     *
     * ````php
     * <?php
     *
     * return [
     *     'width' => 1024,
     *     'height' => 512,
     *     'alt' => 'Some image',
     * ];
     * ````
     *
     * @param string $attributeCacheFolder
     * @return void
     */
    public static function enableAttributeCache(string $attributeCacheFolder): void
    {
        self::$attributeCacheEnabled = true;
        self::$attributeCacheFolder = $attributeCacheFolder;
    }
}
