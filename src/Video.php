<?php

/**
 * Kiwa Source Collection. Handling HTML Audio, Picture and Video elements.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\SourceCollection;

use DOMDocument;
use DOMException;

/**
 * The Video class creates a Video HTML element. For example:
 *
 * ```
 * <video autoplay="autoplay">
 *     <source src="/my-video.mp4" type="video/mp4">
 * </video>
 * ```
 * @see \Kiwa\SourceCollection\Tests\VideoTest
 */
class Video extends AbstractSourceCollection
{
    /**
     * @var array<int, string>|null
     */
    protected static ?array $defaultRootFolderGlobally = null;

    /**
     * These are the next-gen formats, that need to be displayed at first.
     * A browser will take the first possibility, and those should be the newest formats.
     *
     * @var array<int, string>
     */
    protected array $nextGenFormats = [
        'webm',
        'mp4',
    ];

    /**
     * Creates a new video object.
     *
     * @param string $videoSrc                                        The absolute or relative URL of the video.
     *                                                                This path will be displayed when creating the HTML code.
     * @param array<string, string|int|float|bool|null> $videoOptions All attributes for the video tag, like `width`,
     *                                                                `height`, `muted` or `controls`.
     */
    final public function __construct(string $videoSrc, array $videoOptions = [])
    {
        $this->rootFileSrc = $videoSrc;
        $this->addAttributes($videoOptions);
        parent::__construct();
    }

    /**
     * Creates a new video object.
     *
     * @param string $videoSrc                                        The absolute or relative URL of the video.
     *                                                                This path will be displayed when creating the HTML code.
     * @param array<string, string|int|float|bool|null> $videoOptions All attributes for the video tag, like `width`,
     *                                                                `height`, `muted` or `controls`.
     * @return Video
     */
    public static function create(string $videoSrc, array $videoOptions = []): self
    {
        return new static($videoSrc, $videoOptions);
    }

    /**
     * Returns the video as HTML.
     *
     * @return string
     * @throws DOMException
     */
    public function __toString(): string
    {
        return $this->getVideo();
    }

    /**
     * Returns the video as XML.
     *
     * @return DOMDocument
     * @throws DOMException
     */
    public function getVideoDOMDocument(): DOMDocument
    {
        return $this->getCollectionDOMDocument('video');
    }

    /**
     * Renders and returns the video as HTML.
     *
     * @return string
     * @throws DOMException
     */
    public function getVideo(): string
    {
        return $this->getCollection('video');
    }

    /**
     * Enables that related files are automatically searched and added.
     * This method takes place as a global setting.
     *
     * @param string ...$defaultRootFolder One or multiple paths to the folder(s) where the files are stored in.
     */
    public static function enableAutoSearchGenerally(string ...$defaultRootFolder): void
    {
        self::$autoSearchEnabledGlobally = true;
        self::$defaultRootFolderGlobally = array_values($defaultRootFolder);
    }

    /**
     * Returns the auto search folders, that have been set for a specific media type.
     *
     * @return array<int, string>|null
     */
    protected function getAutoSearchFolders(): ?array
    {
        return self::$defaultRootFolderGlobally;
    }
}
