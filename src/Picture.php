<?php

/**
 * Kiwa Source Collection. Handling HTML Audio, Picture and Video elements.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\SourceCollection;

use DOMDocument;
use DOMException;

/**
 * The Picture class creates a Picture HTML element. For example:
 *
 * ```
 * <picture>
 *     <source srcset="/build/images/my-image.webp" type="image/webp"/>
 *     <source srcset="/build/images/my-image.jpg" type="image/jpeg"/>
 *     <img src="/build/images/my-image.jpg"/>
 * </picture>
 * ```
 * @see \Kiwa\SourceCollection\Tests\PictureTest
 */
class Picture extends AbstractSourceCollection
{
    /**
     * These are the next-gen formats, that need to be displayed at first.
     * A browser will take the first possibility, and those should be the newest formats.
     *
     * @var array<int, string>
     */
    protected array $nextGenFormats = [
        'avif',
        'webp',
    ];

    /**
     * Creates a new picture object.
     *
     * @param string $imageSrc                                        The absolute or relative URL of the image.
     *                                                                This path will be displayed when creating the HTML code.
     * @param array<string, string|int|float|bool|null> $imageOptions All attributes for the image, like `width`, `height`,
     *                                                                `alt` or `title`.
     */
    final public function __construct(string $imageSrc, array $imageOptions = [])
    {
        $this->rootFileSrc = $imageSrc;
        $this->setRootFileOptions($imageOptions);
        parent::__construct();
    }

    /**
     * Creates a new picture object.
     *
     * @param string $imageSrc                                        The absolute or relative URL of the image.
     *                                                                This path will be displayed when creating the HTML code.
     * @param array<string, string|int|float|bool|null> $imageOptions All attributes for the image, like `width`, `height`,
     *                                                                `alt` or `title`.
     * @return Picture
     */
    public static function create(string $imageSrc, array $imageOptions = []): self
    {
        return new static($imageSrc, $imageOptions);
    }

    /**
     * Returns the picture as HTML.
     *
     * @return string
     * @throws DOMException
     */
    public function __toString(): string
    {
        return $this->getPicture();
    }

    /**
     * Returns the picture as XML.
     *
     * @return DOMDocument
     * @throws DOMException
     */
    public function getPictureDOMDocument(): DOMDocument
    {
        return $this->getCollectionDOMDocument('picture');
    }

    /**
     * Renders and returns the picture as HTML.
     *
     * @return string
     * @throws DOMException
     */
    public function getPicture(): string
    {
        return $this->getCollection('picture');
    }

    /**
     * Enables that related files are automatically searched and added.
     * This method takes place as a global setting.
     *
     * @param string ...$defaultRootFolder One or multiple paths to the folder(s) where the files are stored in.
     */
    public static function enableAutoSearchGenerally(string ...$defaultRootFolder): void
    {
        self::$autoSearchEnabledGlobally = true;
        self::$defaultRootFolderGlobally = array_values($defaultRootFolder);
    }

    /**
     * Returns the auto search folders, that have been set for a specific media type.
     *
     * @return array<int, string>|null
     */
    protected function getAutoSearchFolders(): ?array
    {
        return self::$defaultRootFolderGlobally;
    }
}
