<?php

/**
 * Kiwa Source Collection. Handling HTML Audio, Picture and Video elements.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\SourceCollection;

class AttributeCacheFileName
{
    private string $fileName;

    public function __construct(string $fileName)
    {
        $this->fileName = $fileName;
    }

    public function __toString(): string
    {
        return (string) $this->getAttributeCacheFileName();
    }

    public function getAttributeCacheFileName(): ?string
    {
        $fileName = mb_strtolower($this->fileName);
        $fileName = str_replace(
            ['_', ' ', '.', DIRECTORY_SEPARATOR],
            '-',
            $fileName
        );
        $fileName = trim($fileName, '-');
        $fileName = preg_replace('/-+/', '-', $fileName);
        return $fileName . '-attributes.php';
    }
}
