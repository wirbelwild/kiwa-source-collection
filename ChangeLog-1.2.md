# Changes in Kiwa Source Collection v1.2

## 1.2.0 2023-08-18

### Added

-   Added the methods `getRootFileSrc`, `getRootFileOptions`, `setRootFileOptions` and `getAttributes`.