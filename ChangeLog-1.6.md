# Changes in Kiwa Source Collection v1.6

## 1.6.0 2024-12-28

### Added

-   Added possibility to load attributes from a cache file.
    To use those files, you need to enable the functionality by calling `AbstractSourceCollection::enableAttributeCache('/cache-folder')`. 
    The cache file name is getting created with and used from the [`AttributeCacheFileName` class](./src/AttributeCacheFileName.php), whereby the Kiwa Source Collection doesn't create those files. 
    You can do that by yourself and store whatever information you want to store.
    The cache file has to be a PHP file that returns a named array, for example:

    ````php
    <?php
    
    return [
        'width' => 1024,
        'height' => 512,
        'alt' => 'Some image',
    ];
    ````