# Changes in Kiwa Source Collection v0.5

## 0.5.0 2021-11-11

### Changed

-   Useless file combinations will be removed now. This means, for example if the root file is a PNG, there will no JPEG be used even if it exists.