# Changes in Kiwa Source Collection v0.6

## 0.6.0 2022-01-18

### Fixed

-   Added missing dependency to support PHP < 8.0.

### Added

-   Added support for `.gif`.